/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type InvalidGasPumpAddress = {
    /**
     * A not activated gas pump address
     */
    address?: string;
    /**
     * The reason why the gas pump address did not get activated
     */
    reason?: string;
}
