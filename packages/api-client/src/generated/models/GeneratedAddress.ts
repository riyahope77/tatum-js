/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type GeneratedAddress = {
    /**
     * Ethereum address
     */
    address?: string;
}
